require 'spec_helper'

describe Comment do

  it { should validate_presence_of(:text) }
  it { should belong_to(:topic) }

  describe 'recursively association' do

    let(:comment) { Comment.create(text: 'A Test Comment') }

    it 'should save a comment as a child of another comment' do

      new_comment = Comment.new(text:'A child comment')
      comment.comments.push(new_comment)

      new_new_comment = Comment.new(text:'A child of child')
      new_comment.comments.push(new_new_comment)

      expect(comment.comments).to_not be_nil
      expect(comment.comments.first).to eq(new_comment)

      expect(new_comment.comments).to_not be_nil
      expect(new_comment.comments.first).to eq(new_new_comment)

    end

  end

  describe 'bad words' do

    let(:comment) { Comment.create(text: 'Filho da puta') }

    it 'should sanitize after create' do
      expect(comment.text).to eq('Filho da ****')
    end

    it 'should sanitize on save' do

      comment.text = 'Minha arma'
      comment.save

      expect(comment.text).to eq('Minha ****')
    end



  end

end
