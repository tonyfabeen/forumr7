require 'spec_helper'

describe CommentsController, type: :controller do

  describe 'GET #index' do

    let!(:topic) do
      topic = Topic.create(title: 'A Topic')
      comment = Comment.new(text: 'A comment for a Topic')
      topic.comments.push(comment)
      topic
    end

    it 'responds successfully with status 200' do
      get :index, id: topic._id
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

  end

end
