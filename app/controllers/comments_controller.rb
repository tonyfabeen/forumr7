class CommentsController < ApplicationController

  before_filter :topic
  respond_to :html

  def index
    @comment  = Comment.new
    @comments = @topic.comments.page(params[:page])
  end

  def create

    @comment = Comment.new(params[:comment].permit(:text))

    if @topic.comments.push(@comment)
      flash[:notice] = t(:comment_created)
      redirect_to(comments_url(@topic))
    else
      flash[:error] =  @topic.errors.full_messages
      render :index
    end

  end

  def reply

    @comment = Comment.find(params[:comment_id])
    @reply   = Comment.new(params[:comment].permit(:text))

    if @comment.comments.push(@reply)
      flash[:notice] = t(:comment_created)
      redirect_to(comments_url(@topic))
    else
      flash[:error] =  @reply.errors.full_messages
      render :index
    end


  end

  private

  def topic
    @topic = Topic.find(params[:id])
  end

end
