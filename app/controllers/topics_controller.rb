class TopicsController < ApplicationController

  respond_to :html

  def index
    topics_per_page
    @topic  = Topic.new
  end

  def create

    @topic = Topic.new(params[:topic].permit(:title))

    if @topic.save
      flash[:notice] = t(:topic_created) if @topic.save
      respond_with(@topic, location: root_path)
    else
      flash[:error] =  @topic.errors.full_messages
      topics_per_page
      render :index
    end

  end

  private

  def topics_per_page
    @topics = Topic.all.page(params[:page])
  end

end
