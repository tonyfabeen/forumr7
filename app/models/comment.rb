class Comment

  include Mongoid::Document
  include Mongoid::Timestamps

  field :text, type: String

  belongs_to :topic
  belongs_to :comment
  has_many   :comments

  validates   :text, presence: true
  before_save :sanitize_text

  protected

  def sanitize_text
    @@blacklist_filter.sanitize(text)
  end

end
