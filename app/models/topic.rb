class Topic

  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String

  has_many :comments

  validates :title, presence: true


end
