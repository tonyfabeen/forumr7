module CommentsHelper

  def render_tree(topic, comment, html='')

    html << "<li>#{comment.text}</li><a href='#' class='toggle_reply'>Reply</a>"
    html << form_for_comment_reply(topic, comment)

    comment.comments.each do |c|
      html << "<ul>"
      render_tree(topic, c, html)
      html << "</ul>"
    end

    html.html_safe

  end

  def form_for_comment_reply(topic, comment)

    route = Rails.application
                 .routes
                 .url_helpers
                 .reply_comment_path(topic._id, comment._id)

    form_for(Comment.new, url: route, html: {class: 'form_reply'}) do |f|
      concat(f.text_area :text)
      concat(f.submit 'Salvar')
    end

  end


end
